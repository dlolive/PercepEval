# Frequently Asked Questions

## General

### What is the minimal code to collect evaluations?

### Is there examples somewhere?

## Administration

### What if I do not include an admin module in my website?

### How do I get results from the SQLite database (flexeval.db) ?

## Authentication

### What are the authentication possibilities?

### What if I do not want the visitors to authenticate?

### How do I set a fixed list of allowed visitors?

## Tests

### How can I record audio from a visitor?

### How can I record video from a visitor?

## Normal pages

